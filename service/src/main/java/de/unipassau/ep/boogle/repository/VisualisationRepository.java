package de.unipassau.ep.boogle.repository;

import org.springframework.stereotype.Repository;

/**
 * This repository connects the service to the visualisation db.
 */
@Repository
public interface VisualisationRepository {
}
