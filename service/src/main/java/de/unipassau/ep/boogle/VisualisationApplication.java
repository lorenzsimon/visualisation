package de.unipassau.ep.boogle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * The entrypoint for the Visualisation-Service
 */

@SpringBootApplication
@EnableDiscoveryClient
public class VisualisationApplication {

	/**
	 * Starts the application.
	 *
	 * @param args The args are unused.
	 */
	public static void main(String[] args) {
		SpringApplication.run(VisualisationApplication.class, args);
	}

}
